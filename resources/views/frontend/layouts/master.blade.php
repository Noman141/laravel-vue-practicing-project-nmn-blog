<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NMN Blog - @yield('title')</title>
    @include('.frontend.partials.styles')
</head>
<body>
<!--================Header Menu Area =================-->
@include('.frontend.partials.header')
<!--================Header Menu Area =================-->

@yield('main-content')

<!--================ Start Footer Area =================-->
@include('.frontend.partials.footer')
<!--================ End Footer Area =================-->

@include('.frontend.partials.scripts')
</body>
</html>
