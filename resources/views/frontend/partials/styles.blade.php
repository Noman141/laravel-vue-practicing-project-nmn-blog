<link rel="icon" href="{{ asset('frontend/img/Fevicon.png"')}}" type="image/png">

<link rel="stylesheet" href="{{ asset('frontend/vendors/bootstrap/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/vendors/fontawesome/css/all.min.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/vendors/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/vendors/linericon/style.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/vendors/owl-carousel/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/vendors/owl-carousel/owl.carousel.min.css')}}">

<link rel="stylesheet" href="{{ asset('frontend/css/style.css')}}">

@yield('styles')
