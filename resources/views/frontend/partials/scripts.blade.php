<script src="{{ asset('frontend/vendors/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{ asset('frontend/vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('frontend/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{ asset('frontend/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{ asset('frontend/js/mail-script.js')}}"></script>

@yield('scripts')

<script src="{{ asset('frontend/js/main.js')}}"></script>
