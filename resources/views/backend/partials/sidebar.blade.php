<ul class="sidebar navbar-nav">
    <li class="nav-item active">
        <a class="nav-link" href="index.html">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Pages</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Login Screens:</h6>
            <a class="dropdown-item" href="login.html">Login</a>
            <a class="dropdown-item" href="register.html">Register</a>
            <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header">Other Pages:</h6>
            <a class="dropdown-item" href="404.html">404 Page</a>
            <a class="dropdown-item" href="blank.html">Blank Page</a>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#manageAdmin" aria-expanded="false" aria-controls="manageAdmin"> <img class="menu-icon" src="{{asset('backend/menu-icon/04.png')}}" alt="menu icon"> <span class="menu-title">Manage Admins</span><i class="fas fa-angle-double-down float-right"></i></a>
        <div class="collapse ml-3" id="manageAdmin">
            <ul class="nav flex-column sub-menu " style="margin-left: 20px">
                <li class="nav-item"> <a class="nav-link" href="{{ route('admin.list') }}">Admin List</a></li>

                @if(Auth::user()->role == 'Super Admin')
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.register') }}">Admin Create</a></li>
                @endif
            </ul>
        </div>
    </li>



    <li class="nav-item">
        <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Charts</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span></a>
    </li>
</ul>
