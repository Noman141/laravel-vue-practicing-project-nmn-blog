@extends('.backend.layouts.master')
@section('title')
    Register Admin
@endsection
@section('style')
    <style>
        .card-body img{
            margin-left: 40%;
            margin-bottom: 20px;
        }
    </style>
@endsection
@section('content')
    @include('.backend.partials.navbar')
    <div id="wrapper">
        @include('.backend.partials.sidebar')
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Update Admin Profile') }}</div>

                        <div class="card-body">
                            @include('.global.message')
                            <img src="{{ asset('backend/images/admin/'.$admin->image) }}" height="100px" width="150px" class="rounded">
                            <form method="POST" action="{{ route('admin.update',$admin->id) }}" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $admin->name }}" required autocomplete="name" autofocus>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $admin->email }}" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ $admin->phone }}" required autocomplete="phone">

                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('User Role') }}</label>

                                    <div class="col-md-6">
                                        <select  id="role" type="text" class="form-control @error('role') is-invalid @enderror" name="role" value="{{ $admin->role }}" required autocomplete="role">
                                            <option disabled selected>Choose...</option>
                                            <option {{ $admin->role === 'Super Admin'? 'selected':'' }} value="Super Admin">Super Admin</option>
                                            <option {{ $admin->role === 'Author'? 'selected':'' }} value="Author">Author</option>
                                            <option {{ $admin->role === 'Writer'? 'selected':'' }} value="Writer">Writer</option>
                                        </select>

                                        @error('role')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Profile Image') }}</label>

                                    <div class="col-md-6">
                                        <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" required autocomplete="image">

                                        @error('image')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Update') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

