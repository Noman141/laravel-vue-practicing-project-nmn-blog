<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NMNBlog - @yield('title')</title>
    @include('.backend.partials.styles')

</head>

<body id="page-top">

@yield('content')

@include('.backend.partials.scripts')

</body>

</html>

