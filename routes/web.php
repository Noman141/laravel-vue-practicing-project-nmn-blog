<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});



Route::group(['prefix' => 'admin'], function (){
    Auth::routes();

    Route::get('/',[\App\Http\Controllers\AdminController::class,'index'])->name('admin.index');
    Route::get('/list',[\App\Http\Controllers\AdminController::class,'adminList'])->name('admin.list');
    Route::get('/edit/{id}',[\App\Http\Controllers\AdminController::class,'edit'])->name('admin.edit');
    Route::put('/update/{id}',[\App\Http\Controllers\AdminController::class,'update'])->name('admin.update');
    Route::delete('/delete/{id}',[\App\Http\Controllers\AdminController::class,'destroy'])->name('admin.delete');
    Route::post('/register',[\App\Http\Controllers\AdminController::class,'register'])->name('admin.register');
    Route::get('/register/verify/{token}',[\App\Http\Controllers\AdminController::class,'verifyAdmin'])->name('admin.registration.verify');

//    admin Category routes
    Route::group(['prefix' => 'category'], function (){
       Route::get('/', [\App\Http\Controllers\AdminCategoryController::class, 'index'])->name('admin.category.index');
       Route::get('/create', [\App\Http\Controllers\AdminCategoryController::class, 'create'])->name('admin.category.create');
       Route::post('/store', [\App\Http\Controllers\AdminCategoryController::class, 'store'])->name('admin.category.store');
       Route::get('/edit/{id}', [\App\Http\Controllers\AdminCategoryController::class, 'edit'])->name('admin.category.edit');
       Route::put('/update/{id}', [\App\Http\Controllers\AdminCategoryController::class, 'update'])->name('admin.category.edit');
       Route::delete('/delete{id}', [\App\Http\Controllers\AdminCategoryController::class, 'destroy'])->name('admin.category.delete');
    });
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'home'])->name('home');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('index');
