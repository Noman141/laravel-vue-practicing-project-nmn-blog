<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Notifications\userVerify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Image;
use File;

class AdminController extends Controller{
    public $image;

    public function __construct(){
        $this->middleware('auth:web');
    }

    public function index(){
        $admins = User::get();
        return view('.backend.pages.index',compact('admins'));
    }

    public function adminList(){
        $admins = User::get();
        return view('backend.pages.admin.index',compact('admins'));
    }

    public function register(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'image' => ['nullable'],
            'role' => ['required', 'string'],
        ]);

        if ($request->hasFile('image')){
            $image = $request->image;
            $imageName = 'NMN-Blog - admin -'.$request->name.'-'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('/backend/images/admin/'.$imageName);
            Image::make($image)->save($location);
            $this->image = $imageName;
        };

        $admin = User::create([
            'name' => $request['name'],
            'slug' => Str::slug($request['name']),
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'phone' => $request['phone'],
            'image' => $this->image,
            'role' => $request['role'],
            'remember_token' => Str::random(50),
        ]);

        $admin->notify(new userVerify($admin, $admin->remember_token));
        session()->flash('success','A confirmation message has sent to the new admins Email');
        return redirect()->route('admin.index');
    }

    public function verifyAdmin($token){
        $admin = User::where('remember_token',$token)->first();
        if (!is_null($admin)){
            $admin->status =1;
            $admin->remember_token = NULL;
            $admin->email_verified_at = time();

            $admin->save();
            session()->flash('success','Your Verification Successful');
            return redirect()->route('login');
        }else{
            session()->flash('errormsg','Sorry! Your token is not matched!!! ');
            return redirect()->route('login');
        }
    }

    public function edit($id){
        $admin = User::find($id);
        return view('.backend.pages.admin.edit', compact('admin'));
    }

    public function update(Request $request, $id){
        $admin = User::find($id);
        $request->validate([
            'name' => "required|string|max:255",
            'email' => "required|string|email|max:255|unique:users,email,$admin->id",
            'phone' => "required|string|unique:users,phone,$admin->id",
            'image' => "nullable",
            'role' => "required|string",
        ]);
        if ($request->hasFile('image')){
            if (File::exists('backend/images/admin/'.$admin->image)){
                File::delete('backend/images/admin/'.$admin->image);
            }

            $image = $request->image;
            $imageName = 'NMN-Blog - admin -'.$request->name.'-'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('/backend/images/admin/'.$imageName);
            Image::make($image)->save($location);
            $this->image = $imageName;
        }
//        $admin->name = $request->name;
//        $admin->slug = Str::slug($request->name);
//        $admin->email = $request->email;
//        $admin->phone = $request->phone;
//        $admin->role = $request->role;
//        $admin->image = $this->image;

        $admin->update([
            'name' => $request['name'],
            'slug' => Str::slug($request['name']),
            'email' => $request['email'],
            'phone' => $request['phone'],
            'image' => $this->image,
            'role' => $request['role'],
        ]);

        session()->flash('success','Admin Update Successfully');
        return redirect()->route('admin.index');
    }

    public function destroy($id){
        $admin = User::find($id);

        if ($admin){
            if (File::exists('backend/images/admin/'.$admin->image)){
                File::delete('backend/images/admin/'.$admin->image);
            }
            $admin->delete();
        }

        session()->flash('success','Admin deleted Successfully');
        return redirect()->route('admin.index');

    }
}
